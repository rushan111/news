package com.rushan.news.security;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Map<String, UserDetailsImpl> users = Map.of(
            "rushan", new UserDetailsImpl(1, "qwerty", "rushan", "USER"),
            "admin", new UserDetailsImpl(2, "amdin", "admin", "ADMIN")
    );

    @Override
    public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
        return users.get(username);
    }
}
