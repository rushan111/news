package com.rushan.news.service;

import com.rushan.news.repository.NewsRepository;
import com.rushan.news.view.NewsView;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class NewsService {

    private final NewsRepository newsRepository;

    public List<NewsView> getAllNews() {
        return newsRepository.findAll().stream()
                .map(it -> NewsView.builder()
                        .id(it.getId())
                        .header(it.getHeader())
                        .text(it.getText())
                        .build())
                .collect(Collectors.toList());
    }

}
