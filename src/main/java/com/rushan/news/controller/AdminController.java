package com.rushan.news.controller;

import com.rushan.news.view.NewsView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @PostMapping
    public String addNews(NewsView news) {
        return "redirect:/admin";
    }

    @PutMapping
    public String updateNews(NewsView news) {
        return "redirect:/admin";
    }

    @PostMapping("/delete/{id}")
    public String deleteNews(@PathVariable int id) {
        return "redirect:/admin";
    }
}
