package com.rushan.news.controller;

import com.rushan.news.exceptions.ClientErrorException;
import com.rushan.news.security.UserDetailsImpl;
import com.rushan.news.service.NewsService;
import com.rushan.news.view.NewsView;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Controller
public class RootController {

    private final NewsService newsService;

    @GetMapping
    public ModelAndView home() {
        var mv = new ModelAndView("home");
        var news = newsService.getAllNews();
        mv.addObject("news", news);
        mv.addObject("userId", getCurrentUser().map(UserDetailsImpl::getId).orElse(-1));
        return mv;
    }

    @GetMapping("/news")
    @ResponseBody
    public List<NewsView> getNews(@RequestParam int num) {
        if (num % 2 == 1) {
            throw new ClientErrorException();
        }
        throw new RuntimeException();
    }

    @GetMapping("/admin")
    public ModelAndView admin() {
        var mv = new ModelAndView("admin");
        var news = newsService.getAllNews();
        mv.addObject("news", news);
        return mv;
    }

    @GetMapping("/sign-in")
    public String login() {
        return "sign-in";
    }

    @GetMapping("/lk/{userId}")
    public ModelAndView lk(@PathVariable int userId) {
        Optional<UserDetailsImpl> currentUser = getCurrentUser();
        if (userId != currentUser.map(UserDetailsImpl::getId).orElse(-1)) {
            throw new AccessDeniedException("You don't have access");
        }
        var mv = new ModelAndView("lk");
        mv.addObject("userName", currentUser.map(UserDetailsImpl::getUsername).orElseThrow());
        return mv;
    }

    private Optional<UserDetailsImpl> getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getName().equals("anonymousUser")) {
            return Optional.empty();
        }
        System.out.println(authentication.getName());
        var details =  (UserDetailsImpl) authentication.getPrincipal();
        return Optional.ofNullable(details);
    }

}
