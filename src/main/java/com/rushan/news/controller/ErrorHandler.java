package com.rushan.news.controller;

import com.rushan.news.exceptions.ClientErrorException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@ControllerAdvice
public class ErrorHandler {


    @ExceptionHandler(Exception.class)
    public ModelAndView handleUnknownError() {
        return new ModelAndView("error");
    }

    @ExceptionHandler(ClientErrorException.class)
    public ResponseEntity<Map<String, Object>> handleClientError() {
        return ResponseEntity.status(406).body(Map.of("message", "client error"));
    }

}
