package com.rushan.news.view;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NewsView {
    private int id;
    private String header;
    private String text;
}
