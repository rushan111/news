package com.rushan.news.controller;

import com.rushan.news.entity.News;
import com.rushan.news.repository.NewsRepository;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@AutoConfigureEmbeddedDatabase
public class WebTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private NewsRepository newsRepository;

    @Test
    public void testGetNews() throws Exception {
        newsRepository.saveAll(List.of(
                News.builder().header("n1").text("t1").build(),
                News.builder().header("n2").text("t2").build()
        ));

        mockMvc.perform(get("/news"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(2)))
                .andExpect(jsonPath("$[0].header", is("n1")))
                .andExpect(jsonPath("$[1].header", is("n2")))
                .andExpect(jsonPath("$[0].text", is("t1")))
                .andExpect(jsonPath("$[1].text", is("t2")));
    }
}
