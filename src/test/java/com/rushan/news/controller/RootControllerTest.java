package com.rushan.news.controller;

import com.rushan.news.entity.News;
import com.rushan.news.repository.NewsRepository;
import com.rushan.news.service.NewsService;
import com.rushan.news.view.NewsView;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@Transactional
@AutoConfigureEmbeddedDatabase
public class RootControllerTest {

    @Autowired
    private NewsService newsService;
    @Autowired
    private NewsRepository newsRepository;

    private RootController rootController;

    @BeforeEach
    public void init() {
        rootController = new RootController(newsService);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHome() {
        newsRepository.saveAll(List.of(
                News.builder().header("Test Header 1").text("test text 1").build(),
                News.builder().header("Test Header 2").text("test text 2").build()
        ));

        var result = rootController.home();

        var news = (List<NewsView>) result.getModel().get("news");
        Assertions.assertNotNull(news);
        Assertions.assertEquals(news.size(), 2);

        var headers = news.stream().map(NewsView::getHeader).collect(Collectors.toSet());
        var texts = news.stream().map(NewsView::getText).collect(Collectors.toSet());

        Assertions.assertTrue(headers.contains("Test Header 1"));
        Assertions.assertTrue(headers.contains("Test Header 2"));
        Assertions.assertTrue(texts.contains("test text 1"));
        Assertions.assertTrue(texts.contains("test text 2"));
    }



}
